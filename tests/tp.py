# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return ("S" * n + "0")


def S(n: str) -> str:
    return f'S{n}'


def addition(a: str, b: str) -> str:
    count = a.count('S')
    count += b.count('S')
    return ("S" * count + "0")


def multiplication(a: str, b: str) -> str:
    count = a.count('S')
    count *= b.count('S')
    return ("S" * count + "0")


def facto_ite(n: int) -> int:
    if (n == 0) :
        return 1
    i = 0
    res = n
    while n > 1 :
        res *= (n - 1)
        n -= 1
    return res


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    if (n <= 1) :
        return  1
    return facto_rec(n - 1) * n


def fibo_rec(n: int) -> int:
    pass


def fibo_ite(n: int) -> int:
    pass


def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass
